<?php
require_once 'support.php';

$countries = [];

$wikiPopulationXml = readWikiToXml('List_of_countries_and_dependencies_by_population');
$wikiInternautsXml = readWikiToXml('List_of_countries_by_number_of_Internet_users');

if (!$wikiPopulationXml && !$wikiInternautsXml) {
    header('Content-Type: application/json');
    echo json_encode(['countries' => []]);
    exit;
}

$trList = $wikiPopulationXml->xpath('/div/table/tbody/tr[td]');
foreach ($trList as $i => $tr) {
    $country = extractCountryName($tr->td[1]->asXML());
    if (!$country) {
        continue;
    }
    $countries[$country] = makeCountry($country);
    $countries[$country]['population'] = extractNumber($tr->td[2]->__toString());
    $countries[$country]['date'] = trim($tr->td[3]->__toString());
}

$trList = $wikiInternautsXml->xpath('/div/table/caption[starts-with(., \'Number of Internet users\')]/../tbody/tr[td]');
foreach ($trList as $i => $tr) {
    $country = extractCountryName($tr->td[0]->asXML());
    if (!$country) {
        continue;
    }
    if (!isset($countries[$country])) {
        $countries[$country] = makeCountry($country);
    }
    $countries[$country]['internauts'] = extractNumber($tr->td[1]->__toString());
}

$globalInternauts = array_reduce($countries, 'sumInternauts');
array_walk($countries, 'addPercents', $globalInternauts);
array_walk($countries, 'formatNumbers');

header('Content-Type: application/json');
echo json_encode(['countries' => array_values($countries)]);
