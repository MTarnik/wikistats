<?php
const WIKI_API_PARSE_URL = 'https://en.wikipedia.org/w/api.php?action=parse&format=json&page=';

function extractNumber($value) {
    return (int)str_replace(',', '', $value);
}

function extractCountryName($value) {
    $value = strip_tags($value);
    $value = preg_replace(['/(\[.*\])/', '/(\(.*\))/'], '', $value);
    $value = htmlentities($value, null, 'utf-8');
    $value = trim(str_replace("&nbsp;", "", $value));
    return $value;
}

function readWikiToXml($pageName)
{
    $rawJson = file_get_contents(WIKI_API_PARSE_URL . $pageName);
    if (!$rawJson) {
        return null;
    }

    $json = json_decode($rawJson, true);
    if (isset($json['error'])) {
        return null;
    }

    return simplexml_load_string($json['parse']['text']['*']);
}

function makeCountry($country)
{
    return [
        'country' => $country,
        'date' => null,
        'population' => null,
        'internauts' => null,
        '%country' => null,
        '%global' => null,
    ];
}

function addPercents(&$country, $key, $total)
{
    if ($country['internauts']) {
        $v = $country['internauts'] / $total * 100;
        $country['%global'] = number_format($v,5) . '%';
        if ($country['population']) {
            $country['%country'] = (round($country['internauts'] / $country['population'] * 100, 0)) . '%';
        }
    }
}

function formatNumbers(&$country)
{
    foreach (['internauts', 'population'] as $key) {
        if ($country[$key]) {
            $country[$key] = number_format($country[$key], 0, ',', ' ');
        }
    }
}

function sumInternauts($total, $country)
{
    return $total + (int)$country['internauts'];
}

