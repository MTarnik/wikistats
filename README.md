**Cel: Stworzenie raportu listującego statystyki dotyczące liczby użytkowników internetu na podstawie danych z Wikipedii**

#### Oczekiwany wynik:
Skrypt PHP, który wypisze tabelkę jak w przykładzie poniżej:

Kolumny: 
* Country (or dependent territory)
* Population Date
* Internet users
* Percentage: internet users in a country / population in a country
* Percentage: internet users in a country / internet users worldwide

Przykładowe wartości:

Poland | 38 424 000 | January 1, 2017 | 26 256 020 | 68% | 0,81937%

#### Szczegóły:
Zadanie polega na napisaniu skryptu w PHP, który:

###### Źródło danych
Wyciągnie dane na temat populacji i liczby użytkowników internetu z poniższych linków:
https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population
https://en.wikipedia.org/wiki/List_of_countries_by_number_of_Internet_users

###### Format danych wynikowych
Wypisze tabelę zgodnie z poniższym opisem

  Kolumny:

1. kolumna A:
   * opis: Country (or dependent territory),
   * źródło danych: population or internet users, 
2. kolumna B:
   * opis: Population,
   * źródło danych: population,
3. kolumna C:
   * opis: Date,
   * źródło danych: population,
4. kolumna D:
   * opis: Internet users,
   * źródło danych: internet_users,
5. kolumna E:
   * opis: Percentage: internet users in a country / population in a country,
   * źródło danych: wartość obliczana [kol. D] / [kol. B],
   * format: procenty, 0 miejsc po przecinku,
6. kolumna F:
   * opis: Percentage: internet users in a country / internet users worldwide,
   * źródło danych: wartość obliczana [kol. D] / [sumę wszystkich wartości w kol. D],
   * format: procenty, 5 miejsc po przecinku.

            Przykład:
            A: Poland
            B: 38 424 000
            C: January 1, 2017
            D: 26 256 020 
            E: 68%
            F: 0,81937%


###### W przypadku problemów z pozyskanymi danymi
W przypadku gdy nazwy krajów pomiędzy tabelami nie są uspójnione (&lt;a href … titile=”country”&gt;&lt;/a&gt;) lub
brakuje danych na temat któregoś kraju w 1 z tabel, należy wypisać je jak w przykładzie poniżej (tzn. nie bawimy się
w porządkowanie nazw, schematów w Wiki)

            Przykład:
            1.  A: Poland
                B: 38 424 000
                C: January 1, 2017
                D: - 
                E: -
                F: -
            2.  A: Polska
                B: -
                C: -
                D: 26 256 020 
                E: -
                F: 0,81937%

###### Niejasności?
Jeśli wystąpią niejasności lub pytania jak ma działać skrypt dla konkretnych przypadków
należy samemu podjąć decyzję i przesłać opis przyjętych założeń wraz z rozwiązaniem
zadania.

#### W ramach rozwiązania należy:
1. Przesłać skrypt PHP.